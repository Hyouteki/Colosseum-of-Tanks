> A desktop 2-player Tank shooter game built on `java` using `libGDX` library. Mock up of the Original Tank stars game.

## Screenshots

![](./screenshots/start-screen.png)
![](./screenshots/choose-tank-screen.png)
![](./screenshots/game-screen.png)
![](./screenshots/pause-screen.png)
![](./screenshots/drop-game-screen.png)
![](./screenshots/game-over-screen.png)
![](./screenshots/select-saved-game-screen.png)

## Highlights
🕹 2 Player tank fighter game aka __Colosseum of Tanks__.<br>
🚜 Can choose between 3 __tanks__.<br>
💣 Having multiple __missiles__.<br>
📦 Having __Supply drops__.<br>
💾 Can __save__ upto 3 games.<br>
🎶 Who doesn't love __music__.

## How to play
1. Clone the repository using `git clone git@gitlab.com:/ninthcircle/colosseum-of-tanks.git`.
2. Build the project in the root directory using `sh gradlew desktop:dist`.
3. Run the game using `sh gradlew desktop:run`.
4. Enjoy the game __"Gladiators"__.

## Courtesy
- __Inspiration:__ Tank stars 
- __Assets:__ Tank stars 
